

#include "../TrigBjetBtagHypoAlgMT.h"

#include "../TrigBjetEtHypoTool.h"
#include "../TrigBjetBtagHypoTool.h"
#include "../TrigSuperRoIBuilderMT.h"


DECLARE_COMPONENT( TrigBjetBtagHypoAlgMT )

DECLARE_COMPONENT( TrigBjetEtHypoTool )
DECLARE_COMPONENT( TrigBjetBtagHypoTool )

DECLARE_COMPONENT( TrigSuperRoIBuilderMT )
